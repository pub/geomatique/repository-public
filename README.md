# Descartes Repository Public

## Repository paquets NPM

Fichier .npmrc
```
 @descartes:registry=https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/17127/packages/npm
```

Trois modules disponibles:
* d-map
* d-editmap
* d-inkmap

## Repository actifacts Maven

Fichier pom.xml

```
<dependency>
  <groupId>fr.gouv.siig.descartes</groupId>
  <artifactId>XXX</artifactId>
  <version>x.x.x</version>
</dependency>
...
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/17127/packages/maven</url>
  </repository>
</repositories>

<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/17127/packages/maven</url>
  </repository>

  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/17127/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```

Deux modules disponibles:
* d-wsmap
* d-jobrunmap

## Repository images Docker
  
* registry.gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/repository-public/docker-d-wsmap:x.x.x
* registry.gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/repository-public/docker-d-jobrunmap:x.x.x
* registry.gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/repository-public/docker-d-demomap:x.x.x
* registry.gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/repository-public/docker-d-nginx:x.x.x

